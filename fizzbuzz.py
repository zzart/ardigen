
def fizzbuzz(nmin, nmax):
    """ Ardigen fizzbuzz function
        nmin: int - lower bound
        nmax: int - higher bound
    """
    if nmin > nmax:
        print("Start value must be less then stop value!")
        return
    else:
        for i in range(nmin, nmax, 1):
            if (i % 3 == 0) and (i % 5 == 0):
                print("fizzbuzz")
            elif i % 3 == 0:
                print("fizz")
            elif i % 5 == 0:
                print("buzz")
            else:
                print(i)
    return True


if __name__ == '__main__':
    print("Welcome to FizzBuzz\n")
    nmin = int(input("Input start value: "))
    nmax = int(input("Input stop value: "))
    fizzbuzz(nmin, nmax)



