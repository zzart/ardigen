import csv
from collections import defaultdict


class Valuator(object):

    def open_csv(self, path, value_name):
        """ generic method to open csv documents and assign values
            to value_name argument
        """
        with open(path) as csv_file:
            self.__dict__[value_name] = []
            reader = csv.DictReader(csv_file)
            for row in reader:
                self.__dict__[value_name].append(row)
        return True

    def convert_to_pl(self, product_item):
        """ converts product_item to PLN currency
            and returns changed dict
        """
        if product_item.get("currency") != "PLN":
            for currency in self.currencies:
                if currency["currency"] == product_item["currency"]:
                    product_item["price"] = float(currency["ratio"]) * int(product_item["price"])
                    product_item["currency"] = "PLN"
                    return product_item
        else:
            return product_item

    def sort_groups_by_matching_id(self, unsorted_group):
        sorted_group = defaultdict(list)
        for row in unsorted_group: 
            sorted_group[row.get("matching_id")].append(row)
        return sorted_group

    def get_matchings(self, matching_id):
        """ return tuple (number of records, igored_count)
        """
        for item in self.matchings:
            if item.get("matching_id") == matching_id:
                return item.get("top_priced_count")
        else:
            raise IndexError

    def write_csv(self, dict_obj, filename):
        """ take any list of dicts and write to csv file
        """
        with open(filename, "w") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=dict_obj[0].keys())
            writer.writeheader()
            for item in dict_obj:
                writer.writerow(item)
        return True

    def get_stats(self, group):
        """ takes list of dics ordered by matching_id
            {"matching_id":[{item1, item2}], .... }
            and returns stats with agregated prices
        """
        result = []
        for key,items in group.items():
            # sort items so we can limit the set by top_price_count
            items.sort(key=lambda x: int(x.get("price")) * int(x.get("quantity")), reverse=True)
            limit_items = int(self.get_matchings(items[0].get("matching_id")))
            filtered_items = items[:limit_items]
            ignored_product_count = len(items) - limit_items
            stats = {"matching_id": None, "total_highest_price": 0, "avg_price": 0,
                     "currency":"PLN", "ignored_product_count": ignored_product_count}
            avg = 0
            for item in filtered_items:
                self.convert_to_pl(item)
                avg += int(item.get("price"))
                if int(item.get("price")) * int(item.get("quantity")) > stats.get("total_highest_price"):
                    stats["matching_id"] = item.get("matching_id")
                    stats["total_highest_price"] = int(item.get("price")) * int(item.get("quantity"))
            stats["avg_price"] = round(avg / len(items), 2)
            result.append(stats)
        return result

    def main(self):
        self.open_csv("currencies.csv", "currencies")
        self.open_csv("data.csv", "data")
        self.open_csv("matchings.csv", "matchings")
        output = self.get_stats(self.sort_groups_by_matching_id(self.data))
        self.write_csv(output, "top_products.csv")
        return True



if __name__ == "__main__":
    valuator = Valuator()
    valuator.main()
