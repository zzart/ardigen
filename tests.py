import unittest
from valuation import Valuator


class TestValuator(unittest.TestCase):
    
    def setUp(self):
        self.v = Valuator()
        self.v.open_csv("data.csv", "data")
        self.v.open_csv("matchings.csv", "matchings")
        self.v.open_csv("currencies.csv", "currencies")

    def test_open_csv(self):
        self.assertTrue(self.v.open_csv("currencies.csv", "currencies"))

    def test_write_csv(self):
        self.assertTrue(self.v.write_csv([{"test":1}], "test.csv"))

    def test_main(self):
        self.assertTrue(self.v.main(), True)

    def test_converter(self):
        input =  {'currency': 'GBP', 'quantity': '3', 'price': '1400'}
        output = {'currency': 'PLN', 'quantity': '3', 'price': 3360.0}
        self.assertEqual(self.v.convert_to_pl(input), output)
        
    def test_sort_groups(self):
        input =  [{'matching_id': '1'},{'matching_id': '2'}]
        output =  {'1':[{'matching_id': '1'}],'2':[{'matching_id': '2'}]}
        self.assertEqual(self.v.sort_groups_by_matching_id(input), output)
    
    def test_get_matchings(self):
        self.assertEqual(self.v.get_matchings('2'),'2')
        
    def test_get_stats(self):
        input = {'3': [{'price': '1000', 'matching_id': '3', 'currency': 'GBP', 'id': '1', 'quantity': '2'}, {'price': '1400', 'matching_id': '3', 'currency': 'EU', 'id': '5', 'quantity': '4'}, {'price': '630', 'matching_id': '3', 'currency': 'GBP', 'id': '7', 'quantity': '5'}]}
        output = [{'avg_price': 2284.0, 'total_highest_price': 11760, 'matching_id': '3', 'ignored_product_count': 0, 'currency': 'PLN'}] 
        self.assertEqual(self.v.get_stats(input), output)
        
        
    
        

if __name__ == "__main__":
    unittest.main()

